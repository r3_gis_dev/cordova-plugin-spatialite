/*
 * PhoneGap is available under *either* the terms of the modified BSD license *or* the
 * MIT License (2008). See http://opensource.org/licenses/alphabetical for full text.
 *
 * Copyright (c) 2005-2010, Nitobi Software Inc.
 * Copyright (c) 2010, IBM Corporation
 */
package com.phonegap.plugin.sqlitePlugin;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;

//import java.lang.Number;

import java.util.HashMap;

import org.apache.cordova.*;


import android.database.Cursor;

//import android.database.sqlite.*;
import jsqlite.*;
import java.util.*;

//import android.util.Base64;
import android.util.Log;




public class SQLitePlugin extends CordovaPlugin
{
	/**
	 * Multiple database map.
	 */
	HashMap<String, Database> dbmap;

	/**
	 * Constructor.
	 */
	public SQLitePlugin() {
		dbmap = new HashMap<String, Database>();
	}

	/**
	 * Executes the request and returns PluginResult.
	 *
	 * @param action
	 *            The action to execute.
	 *
	 * @param args
	 *            JSONArry of arguments for the plugin.



	 *
	 * @param cbc
	 *            Callback context from Cordova API (not used here)
	 *
	 */
	@Override
	public boolean execute(String action, JSONArray args, CallbackContext cbc)
	{
		try {
			if (action.equals("open")) {
				JSONObject o = args.getJSONObject(0);
				String dbname = o.getString("name");
				String dbpath = "";
				
				if(o.has("path")) {
					dbpath = o.getString("path");
				}

				this.openDatabase(dbname, dbpath);
				
				// add full database path to callback success
				File dbFile = this.getDatabasePath(dbname, dbpath);
				JSONObject response = new JSONObject();
				response.put("path", dbFile.getParent());

				cbc.success(response);
			}
			else if (action.equals("close")) {
				this.closeDatabase(args.getString(0));
			}
			else if (action.equals("executePragmaStatement"))
			{
				String dbName = args.getString(0);
				String query = args.getString(1);

				//Cursor myCursor = this.getDatabase(dbName).rawQuery(query, null);
				//this.processPragmaResults(myCursor, id);
			}
			else if (action.equals("executeSqlBatch"))
			{
				String[] 	queries 	= null;
				String[] 	queryIDs 	= null;
				String 		trans_id 	= null;
				JSONObject 	a 			= null;
				JSONArray 	jsonArr 	= null;
				int 		paramLen	= 0;
				JSONArray[] 	jsonparams 	= null;


				String dbName = args.getString(0);
				JSONArray txargs = args.getJSONArray(1);

				if (txargs.isNull(0)) {
					queries = new String[0];
				} else {
					int len = txargs.length();
					queries = new String[len];
					queryIDs = new String[len];
					jsonparams = new JSONArray[len];

					for (int i = 0; i < len; i++)
					{
						a 			= txargs.getJSONObject(i);
						queries[i] 	= a.getString("query");
						queryIDs[i] = a.getString("query_id");
						trans_id 	= a.getString("trans_id");
						jsonArr 	= a.getJSONArray("params");
						paramLen	= jsonArr.length();
						jsonparams[i] 	= jsonArr;
					}
				}
				if(trans_id != null)
					this.executeSqlBatch(dbName, queries, jsonparams, queryIDs, trans_id);
				else
					Log.d("SQLitePlugin", "error: null trans_id");
			}


			return true;
		} catch (JSONException e) {

			// TODO: signal JSON problem to JS

			return false;
		}
	}

	/**
	 *
	 * Clean up and close all open databases.
	 *
	 */
	@Override
	public void onDestroy() {


		while (!this.dbmap.isEmpty()) {
			String dbname = this.dbmap.keySet().iterator().next();

			this.closeDatabase(dbname);
			this.dbmap.remove(dbname);
		}
	}

	// --------------------------------------------------------------------------
	// LOCAL METHODS
	// --------------------------------------------------------------------------

	private File getDatabasePath(String dbname, String path) {
		//String dbfilePath;
        File dbPath;
		File dbFile;
		
		Log.d("SQLitePlugin", "getDatabasePath(); arguments, name: " + dbname + ", path: " + path);
		if(path.length() == 0) {
            dbPath = this.cordova.getActivity().getExternalFilesDir(null);
			dbFile = new File(dbPath, dbname + ".db");
		    //dbfilePath = dbFile.getAbsolutePath();
		} else {
			//dbfilePath = path + dbname + ".db";
			dbFile = new File(path,  dbname + ".db");
		}
		
		Log.d("SQLitePlugin", "getDatabasePath(); db: " + dbFile.getAbsolutePath());
		
		return dbFile;
	}
	
	/**
	 * Open a database.
	 *
	 * @param dbname
	 *            The name of the database-NOT including its extension.
	 *
	 * @param password
	 *            The database password or null.
	 *
	 */
	private void openDatabase(String dbname, String path)
	{
		if (this.getDatabase(dbname) != null) this.closeDatabase(dbname);
		
        Database mydb = new jsqlite.Database();
        try {
        	mydb.open(this.getDatabasePath(dbname, path).getAbsolutePath(), jsqlite.Constants.SQLITE_OPEN_READWRITE);
        } catch(jsqlite.Exception ex) {
			ex.printStackTrace();
			Log.d("SQLitePlugin", "SQLitePlugin.openDatabase(): Error=" +  ex.getMessage());
        }

		dbmap.put(dbname, mydb);
	}

	/**
	 * Close a database.
	 *
	 * @param dbName
	 *            The name of the database-NOT including its extension.
	 *
	 */
	private void closeDatabase(String dbName)
	{
		Database mydb = this.getDatabase(dbName);

		if (mydb != null)
		{
			try {
				mydb.close();
			} catch(jsqlite.Exception ex) {
				ex.printStackTrace();
				Log.d("SQLitePlugin", "SQLitePlugin.closeDatabase(): Error=" +  ex.getMessage());
			}
			this.dbmap.remove(dbName);
		}
	}

	/**
	 * Get a database from the db map.
	 *
	 * @param dbname
	 *            The name of the database.
	 *
	 */
	private Database getDatabase(String dbname)
	{
		return dbmap.get(dbname);
	}

	/**
	 * Executes a batch request and sends the results via sendJavascriptCB().
	 *
	 * @param dbname
	 *            The name of the database.
	 *
	 * @param queryarr
	 *            Array of query strings
	 *
	 * @param jsonparams
	 *            Array of JSON query parameters
	 *
	 * @param queryIDs
	 *            Array of query ids
	 *
	 * @param tx_id
	 *            Transaction id
	 *
	 */

    
	public void executeSqlBatch(String dbname, String[] queryarr, JSONArray[] jsonparams, String[] queryIDs, String tx_id) {
		Log.d("SQLitePlugin", "executeSqlBatch");
		
		Database mydb = this.getDatabase(dbname);
		
		try {
			// TODO: this.myDb.beginTransaction();
			String query = "";
			String query_id = "";
			int len = queryarr.length;
			for (int i = 0; i < len; i++) {
				query = queryarr[i];
				query_id = queryIDs[i];
				if (query.toLowerCase().startsWith("insert") && jsonparams != null) {
					String[] params = null;
					
					if (jsonparams != null) {
						params = new String[jsonparams[i].length()];
						for (int j = 0; j < jsonparams[i].length(); j++) {
							params[j] = jsonparams[i].getString(j);
							if(params[j] == "null") // XXX better check
								params[j] = "";
						}
					}
					
					Log.d("SQLitePlugin", "Database.get_table query=" + query + ", params" + params.toString());
					mydb.exec(query, new TableResult(), params);
					
					
					long insertId = mydb.last_insert_rowid();

					String result = "{'insertId':'" + insertId + "'}";
					
                    this.sendJavascriptCB("window.SQLitePluginTransactionCB.queryCompleteCallback('" + tx_id + "','" + query_id + "', " + result + ");");
				} else {
					String[] params = null;
					
					if (jsonparams != null) {
						params = new String[jsonparams[i].length()];
						for (int j = 0; j < jsonparams[i].length(); j++) {
							params[j] = jsonparams[i].getString(j);
							if(params[j] == "null") // XXX better check
								params[j] = "";
						}
					}
					
					// example 0: regioni
					Log.d("SQLitePlugin", "Database.get_table query=" + query + ", params" + params.toString());
					
					jsqlite.TableResult tblFmt = mydb.get_table(query, params);
					
					Log.d("SQLitePlugin", "Database.get_table size=" + tblFmt.rows.size());
					
					this.processResults(tblFmt, query_id, tx_id);
					
					tblFmt.clear();
				}
			}
			// TODO: this.myDb.setTransactionSuccessful();
		}
		catch (jsqlite.Exception ex) {
			ex.printStackTrace();
			Log.d("SQLitePlugin", "SQLitePlugin.executeSqlBatch(): Error=" +  ex.getMessage());
			this.sendJavascriptCB("window.SQLitePluginTransactionCB.txErrorCallback('" + tx_id + "', '"+ex.getMessage()+"');");
		} catch (JSONException ex) {
			ex.printStackTrace();
			Log.d("SQLitePlugin", "SQLitePlugin.executeSqlBatch(): Error=" +  ex.getMessage());
			this.sendJavascriptCB("window.SQLitePluginTransactionCB.txErrorCallback('" + tx_id + "', '"+ex.getMessage()+"');");
		}
		finally {
			//mydb.endTransaction();
			Log.d("SQLitePlugin", tx_id);
			this.sendJavascriptCB("window.SQLitePluginTransactionCB.txCompleteCallback('" + tx_id + "');");
		}
	}

	/**
	 * Process query results.
	 *
	 * @param cur
	 *            Cursor into query results
	 *
	 * @param query_id
	 *            Query id
	 *
	 * @param tx_id
	 *            Transaction id
	 *
	 */

	public void processResults(jsqlite.TableResult cur, String query_id, String tx_id) {
		String result = "[]";
		JSONArray fullresult = new JSONArray();
		
		Vector<String[]> vector = cur.rows;
		for (String[] element : vector) {
			JSONObject row = new JSONObject();
			
			for (int j = 0; j < element.length; j++) {
				try {
					//row.put(cur.column[j].toLowerCase(), element[j]);
					row.put(cur.column[j], element[j]);
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
			
			fullresult.put(row);
		}
		
		result = fullresult.toString();
		
		if(query_id.length() > 0)
			this.sendJavascriptCB("window.SQLitePluginTransactionCB.queryCompleteCallback('" +
					tx_id + "','" + query_id + "', " + result + ");");
	}

	/**
	 * Process query results.
	 *
	 * @param cur
	 *            Cursor into query results
	 *
	 * @param id
	 *            Caller db id
	 *
	 */
	private void processPragmaResults(jsqlite.TableResult cur, String id)
	{
		//String result = this.results2string(cur);

		//this.sendJavascriptCB("window.SQLitePluginCallback.p1('" + id + "', " + result + ");");
	}

	/**
	 * Convert results cursor to JSON string. -- SEMBRA INUTILIZZATA
	 *
	 * @param cur
	 *            Cursor into query results
	 *
	 * @return results in string form
	 *
	 */
	private String results2string(Cursor cur)
	{
		String result = "[]";

		// If query result has rows
		if (cur.moveToFirst()) {
			JSONArray fullresult = new JSONArray();



			String key = "";
			int colCount = cur.getColumnCount();

			// Build up JSON result object for each row
			do {
				JSONObject row = new JSONObject();


				try {

					for (int i = 0; i < colCount; ++i) {
						key = cur.getColumnName(i);

						// for old Android SDK remove lines from HERE:

							row.put(key, cur.getString(i));
						
					}

					fullresult.put(row);

				} catch (JSONException e) {
					e.printStackTrace();
				}



			} while (cur.moveToNext());

			result = fullresult.toString();
		}






		return result;
	}


	/**
	 * Send Javascript callback.
	 *
	 * @param cb
	 *            Javascript callback command to send
	 *
	 */
	private void sendJavascriptCB(String cb)
	{
		this.webView.sendJavascript(cb);
	}
}
